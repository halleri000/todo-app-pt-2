import React from "react";
import TodoItem from "./TodoItem";

const TodoList = ({ todos, toggleComplete, handleDelete }) => {
  return (
    <section className="main">
      <ul className="todo-list">
        {todos.map((todo) => (
          <TodoItem
            title={todo.title}
            completed={todo.completed}
            toggleComplete={toggleComplete(todo.id)}
            handleDelete={handleDelete(todo.id)}
          />
        ))}
      </ul>
    </section>
  );
};

export default TodoList;
